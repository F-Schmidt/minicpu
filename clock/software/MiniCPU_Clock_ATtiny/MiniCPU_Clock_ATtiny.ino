//------------------------------------------------------------------------------------------------
// This software implements an adjustable clock on an ATtiny and is part of the MiniCPU project.
// When used with the MiniCPU mainboard, this small module can be used to automate the clock, so
// that only the switches have to be flipped, but the clock does not have to be operated every
// time an operation should be carried out.
//
// This program offers various setting options to support diferent hardware variants - either
// energy-saving variants or versions with more feautres, such as status LEDs and error
// detection. You can also configure whether a high level or low level clock should be
// implemented.
//
//------------------------------------------------------------------------------------------------
//
// Developed by
//    > Falko Schmidt, University of Ulm
//
// Version
//    > 1.0 (01/2021)
//
// Project
//   > MiniCPU Clock-Addon
//
//------------------------------------------------------------------------------------------------
//
//                                  ATtiny 25 / 45 / 85
//
//                                         +----+
//                                   PB5  1|°   |20  VCC
//      Clock connection pin <- (D3) PB3  2|    |19  PB2 (D2) <- Delay selection pin J1
//               Status LED <-  (D4) PB4  3|    |18  PB1 (D1) <- Delay selection pin J2
//                                   GND  4|    |17  PB0 (D0) <- Delay selection pin J3
//                                         +----+
//
//------------------------------------------------------------------------------------------------
// Definitions for convenient and understandable configuration
#define       DISABLED                               0        // DO NOT CHANGE!
#define       ENABLED                                1        // DO NOT CHANGE!

//************************************************************************************************
// CONFIGURATION START - YOU MAY CHANGE THE FOLLOWING VALUES ACCORDING TO YOUR NEEDS
//************************************************************************************************
//************************************************************************************************
// PWM_HIGH_LEVEL_CLOCK:
//------------------------------------------------------------------------------------------------
// If you want a high level clock, then you need to set this value to '1'. The result clock
// will create this kind of signal:
//
// +3v3          ___          ___          ___         ___
//              |   |        |   |        |   |       |   |
//              |   |        |   |        |   |       |   |
//  GND ________|   |________|   |________|   |_______|   |________
//
//
// If you want a low level clock, then you need to set this value to '0'. The result clock
// will create this kind of signal:
//
// +3v3 ________     ________     ________     _______     ________
//              |   |        |   |        |   |       |   |
//              |   |        |   |        |   |       |   |
//  GND         |___|        |___|        |___|       |___|
//
//------------------------------------------------------------------------------------------------
#define       PWM_HIGH_LEVEL_CLOCK                   0

//------------------------------------------------------------------------------------------------
// Configure the clock:
// CLOCK_ACTIVE_TIME_MILLIS (default: 100):
//    -> The duration of the clock pulse. If you for example configured a high level clock, then
//       the 3v3-pulse will last about CLOCK_ACTIVE_TIME_MILLIS milliseconds.
//
// CLOCK_CONNECTION_PIN (default: 3):
//    -> The pin, which will output the clock signal. Please reference the schematic at the top
//       of this file to find out, which pin you are using.
//------------------------------------------------------------------------------------------------
#define       CLOCK_ACTIVE_TIME_MILLIS                100
#define       CLOCK_CONNECTION_PIN                    3

//------------------------------------------------------------------------------------------------
// Configure the pins, that will be used to configure the clock period time.
// If a jumper is connected, which corresponds to a '1' in the truth table below, then the
// input pin will be connected to GND. If no jumper is set, then an external pull-up
// resistor pulls the pin to +3v3.
//
//              J0   |   J1   |   J2   ||   Delay time (ms)
//          ---------------------------------------------------
//              0        0        0              3000
//              0        0        1              5000
//              0        1        0              7500
//              0        1        1             10.000
//              1        0        0             12.500
//              1        0        1             17.500
//              1        1        0             20.000
//              1        1        1             30.000
//
//------------------------------------------------------------------------------------------------
#define       DELAY_SELECTION_J0                      2
#define       DELAY_SELECTION_J1                      1
#define       DELAY_SELECTION_J2                      0

//------------------------------------------------------------------------------------------------
// Configure the status LED:
// STATUS-LED_PIN (default: 4):
//    -> The pin, at which the status LED is connected. Use the schematic at the top of this
//       file to get the correct pin.
//
// ACTIVATE_STATUS_LED_ON_ERROR (default: ENABLED):
//    -> If there is an error while reading the jumper states and the microcontroller calculates
//       an invalid setting value, then the LED will blink at a low frequency. Set this value
//       to ENABLED, if you want to activate this small security feature. If you do not want it,
//       then set this value to DISABLED.
//
// ACTIVATE_STATUS_LED_ON_CLOCK_PULSE (default: ENABLED):
//    -> If this value is set to ENABLED, then the LED will behave according to the clock output.
//       Please notice, that if the clock is low active, the LED will be activated most of the
//       time. If you do not wish to have a clock indication, then set this value to DISABLED.
//------------------------------------------------------------------------------------------------
#define       STATUS_LED_PIN                          4

#define       ACTIVATE_STATUS_LED_ON_ERROR            ENABLED
#define       ACTIVATE_STATUS_LED_ON_CLOCK_PULSE      ENABLED

//************************************************************************************************
//************************************************************************************************
// CODE START - DO NOT CHANGE!
//************************************************************************************************
// Defined delay intervals according to the silk screen on the PCB
//const unsigned long DELAY_TIMES[8] = {3000, 5000, 7500, 10000, 12500, 17500, 20000, 30000};
const uint8_t DELAY_INTERVALS[8] = {6, 10, 15, 20, 25, 35, 40, 60}; //ggt ist 500

// Variable to store waiting_time. Sadly the loop-function can not receive any amount of
// arguments. Instead this gloabl variable is used to store the delay time.
uint8_t waiting_interval;


void long_delay(uint8_t wi) {
  for(uint8_t i = 0; i < wi; i++) {
    delay(500);
  }
}
/*
 * This function will be executed once, whenever the microcontroller starts up.
 * All connected pins will be defined to be either inputs or outputs and the
 * jumper positions will be read. The configured delay time will be determined.
 */
void setup(void) {
    pinMode(STATUS_LED_PIN, OUTPUT);
  
    // Set clock pin to be an output and set it to active HIGH
    pinMode(CLOCK_CONNECTION_PIN, OUTPUT);
    #ifdef PWM_HIGH_LEVEL_CLOCK == ENABLED
        digitalWrite(CLOCK_CONNECTION_PIN, LOW);
    #else
        digitalWrite(CLOCK_CONNECTION_PIN, HIGH);
    #endif

    // All settings pins will be inputs
    pinMode(DELAY_SELECTION_J0, INPUT_PULLUP);
    pinMode(DELAY_SELECTION_J1, INPUT_PULLUP);
    pinMode(DELAY_SELECTION_J2, INPUT_PULLUP);

    // Set global variable according to setting on PCB.
    // Please notice, that the loop-function can not take arguments.
    uint8_t delay_select = 0;
    delay_select |= digital_read_and_shift(DELAY_SELECTION_J0, 2);
    delay_select |= digital_read_and_shift(DELAY_SELECTION_J1, 1);
    delay_select |= digital_read_and_shift(DELAY_SELECTION_J2, 0);

    #ifdef ACTIVATE_STATUS_LED_ON_ERROR == ENABLED
        if (delay_select < 0 || delay_select >= 8) {
            for (;;) {
                digitalWrite(STATUS_LED_PIN, HIGH);
                delay(200);
                digitalWrite(STATUS_LED_PIN, LOW);
                delay(200);
            }
        }
    #endif

    //just for debugging:
    digitalWrite(STATUS_LED_PIN, HIGH);
    delay(5000);

    waiting_interval = DELAY_INTERVALS[delay_select];
}


/*
 * This function will loop forever. One 'run' corresponds to a single period.
 * First the microcontroller will wait the amount of milliseconds set with the
 * jumpers on the PCB. After that (depending on the settings made above) either
 * a low clock pulse or a high clock pulse will be created. Depending in the 
 * settings, the status LED may light up according to the created clock pulse.
 */
void loop(void) {
  
    // Wait according to the set delay time
    
    long_delay(waiting_interval);
    
    #ifdef PWM_HIGH_LEVEL_CLOCK == 1
        #ifdef ACTIVATE_STATUS_LED_ON_ERROR == ENABLED
            // Activate LED according to high clock pulse
            digitalWrite(STATUS_LED_PIN, HIGH);
        #endif
        // Create high clock pulse
        digitalWrite(CLOCK_CONNECTION_PIN, HIGH);
        delay(CLOCK_ACTIVE_TIME_MILLIS);
        digitalWrite(CLOCK_CONNECTION_PIN, LOW);
        #ifdef ACTIVATE_STATUS_LED_ON_CLOCK_PULSE == ENABLED
            // Deactivate LED after clock pulse
            digitalWrite(STATUS_LED_PIN, LOW);
        #endif
    #elif PWM_HIGH_LEVEL_CLOCK == 0
        #ifdef ACTIVATE_STATUS_LED_ON_CLOCK_PULSE == ENABLED
            // Deactivate LED according to low clock pulse
            digitalWrite(STATUS_LED_PIN, LOW);
        #endif
        // Create low clock pulse
        digitalWrite(CLOCK_CONNECTION_PIN, LOW);
        delay(CLOCK_ACTIVE_TIME_MILLIS);
        digitalWrite(CLOCK_CONNECTION_PIN, HIGH);
        #ifdef ACTIVATE_STATUS_LED_ON_CLOCK_PULSE == ENABLED
            // Activte LED after clock pulse
            digitalWrite(STATUS_LED_PIN, HIGH);
        #endif
    #endif
}


/*
 * Reads a pin by calling 'digitalRead'. The read value will be inverted and
 * then shifted 'sl' positions to the left.
 * 
 * This function is used to read the states of the jumpers on the PCB to
 * determine the configured delay time.
 */
uint8_t digital_read_and_shift(const uint8_t pin, const uint8_t sl) {
    return ((digitalRead(pin) == LOW) << sl);
}
