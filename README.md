# MiniCPU &ndash; The first interstellar open source CPU project for teaching purposes

This project implements a minimal CPU that can load registers and perform simple arithmetic operations such as addition and subtraction.
The open source TinyFPGA Bx is used to synthesize the CPU. However, since this does not have enough pins for the direct connection of 7 segment displays, these are controlled and updated by external shift registers.
For simple soldering work, SMD was not used.

## Getting started &ndash; Clone and view the project
If you want to build the project or take a look at the files, you first need to clone this project. First of all, navigate to the directory, where the project files should be stored. After that execute the following command in a terminal:
    
    git clone https://gitlab.com/F-Schmidt/minicpu.git

To take a look at the schematic and the pcb-design, you need to install [KiCAD](https://kicad.org/). KiCAD is a cross platform and Open Source electronics design tool. To open this project search for the project file (.pro) and double-click or follow these steps:

    File -> Open project -> Choose the .pro file

If some components or footprints are not shown at all or shown incorrect, you need to import the symbol libraries. Still in the main KiCAD window, navigate to

    Preferences -> Manage Symbol Libraries -> Project specific libraries 
    -> Add all .lib files in the directory containing the project

After that you also need to import the footprint libraries for the used components. Therefore you want to do:

    Preferences -> Manage Footprint Libraries -> Project specific libraries 
    -> Add all directories ending with .pretty located inside of the project directory
    
Now you should be set and you can review the schematic as well as the pcb-layout. If the changes do not yet apply, then you might want to restart KiCAD and reopen the project files.


## MiniCPU
This project uses a TinyFPGA Bx board to implement a MiniCPU. Due to the lack of GPIO pins, eight external 8-bit shift registers are also required to visualize the register content of the four internal registers (%0 to %3). The main board has the dimensions 115x205 mm. Except for two transistors, this design uses only THT components to simplify soldering.
Other features:
	
* Triple power supply: USB, power jack and screw terminal can be used to connect a power supply unit. In addition, the power jack and screw terminal connections are protected against reverse polarity.
* SPI interface: For expansion, additional boards can be connected directly to the FPGA board using a 6-pin ribbon cable.
* Adjustable LEDs: The LEDs for displaying power and clock signals can be activated or deactivated with a jumper as required.
* Jumper storage: The board offers space for up to 10 jumpers. If one is lost, it can be replaced immediately.
* Test points: Before connecting the FPGA and other components, the board can be tested. Not only the 3.3V line, but also other important signals are equipped with a small test pad. A measurement with an oscilloscope or a multimeter is thus easily possible.
* Clock extension: The main board has a 21x31 mm area to accommodate a clock module. It is possible to use the clock module in this repository. Alternatively, your own modules can also be implemented. The additional board has the connections 3.3V, GND and the clock signal. Please note: If an automatic clock module is used, the clock switch on the main board has no effect anymore. It is therefore advisable to make the additional circuit board deactivatable.

![](./cpu/pcb-design/HPC_MiniCPU_FRONT.png)
![](./cpu/pcb-design/HPC_MiniCPU_BACK.png)

## Clock-module for MiniCPU
Tired of pressing that clock button over and over again? Then feel free to use the developed clock-module which can be configured to give pulses in a specific frequency. Check out the PCB files, that contain all relevant information.


## Help developing this project
If you like what you see here and you are interested in the first version (which will be updated with a lot of bug fixes and more features soon) then please consider donating. This helps me to create more prototypes and buy better electronic components. Also I am planning on releasing a website to share information about digital circuits and electronics in general.

[![Donate via PayPal](https://www.paypalobjects.com/en_US/DK/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/donate?hosted_button_id=Q4Q78JBPUZBDJ)

---

##### Enjoy building :)
##### - Falko Schmidt 
