/*
 * Copyright (c) 2020. Falko Schmidt
 * Structural description of a buffered CarryRippleAdder. The output of
 * the adder will be written into a register and output on the next
 * clock pulse.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _BufferedCarryRippleAdder_v
`define _BufferedCarryRippleAdder_v

`include "CarryRippleAdder.v"
`include "Register.v"

module BufferedCarryRippleAdder (input [7:0]VEC_A,
                                input [7:0]VEC_B,
                                input C_IN,
                                input CLK,
                                output [7:0] VEC_S,
                                output C_OUT);

wire [7:0]INTERNAL_DATA;

CarryRippleAdder CR0 (.VEC_A(VEC_A), .VEC_B(VEC_B), .C_IN(C_IN),
                      .VEC_S(INTERNAL_DATA), .C_OUT(C_OUT));
Register RA0 (.D(INTERNAL_DATA), .CLK(CLK), .D_OUT(VEC_S));

endmodule //BufferedCarryRippleAdder

`endif
