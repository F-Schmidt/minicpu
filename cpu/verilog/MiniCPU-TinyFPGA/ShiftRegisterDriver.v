/*
 * Copyright (c) 2020. Falko Schmidt
 * Behavior-based description a shiftregister driver, which will output
 * 16 bits to two serial registers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _ShiftRegisterDriver_v
`define _ShiftRegisterDriver_v

 module ShiftRegisterDriver (
  input clk_16Mhz,
  input[15:0] data,
  output shiftout_clock,
  output reg shiftout_latch,
  output shiftout_data);

reg [15:0] shift_reg = 0;
assign shiftout_data = shift_reg[15];

// Clock divider
reg [1:0] divider;
wire clk = divider[1];

// Increment divider by 1 whenever a positive edge (0->1) occurs
always @(posedge clk_16Mhz) begin
    divider <= divider + 1;
end

// Shiftout clock is assigned to the slow clock
assign shiftout_clock = clk;

// Delay counter variable
reg [20:0] delay_counter;

// Bit counter (will count from 0 to 15)
reg [3:0] bit_counter = 0;

always @(negedge clk) begin
    // If delay counter is zero, then the data will be shifted out to
    // the registers and will be displayed.
    if (delay_counter == 0) begin
      shift_reg <= shift_reg << 1;
      bit_counter <= bit_counter + 1;
      if (bit_counter == 15) begin
        // Show the new value (latch will be set to 1) and reset the counter.
        shiftout_latch <= 1;
        delay_counter <= 1;
      end
    end else begin
      delay_counter <= delay_counter + 1;
      // If the delay counter is on maximum value, then the data will be
      // shifted out.
      if (&delay_counter) begin
        shiftout_latch <= 0;
        // Output register content
        shift_reg <= data;
      end
    end
end
endmodule

`endif
