/*
 * Copyright (c) 2020. Falko Schmidt
 * Behavior-based description of a 7 segment encoder. Depending on the
 * input, the according binary codes will be output to display the correct
 * number on a 7 segment display. Note, that the value '12' will be
 * displayed as small 'c' (not 'C').
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _HexDisplayDriver_v
`define _HexDisplayDriver_v

module HexDisplayDriver (
  input[3:0] value,
  output reg[7:0] hex_code);

always @ ( value ) begin
  case (value)
    4'b0000: hex_code <= 8'b11100111;
    4'b0001: hex_code <= 8'b00100001;
    4'b0010: hex_code <= 8'b11001011;
    4'b0011: hex_code <= 8'b01101011;
    4'b0100: hex_code <= 8'b00101101;
    4'b0101: hex_code <= 8'b01101110;
    4'b0110: hex_code <= 8'b11101110;
    4'b0111: hex_code <= 8'b00100011;
    4'b1000: hex_code <= 8'b11101111;
    4'b1001: hex_code <= 8'b01101111;
    4'b1010: hex_code <= 8'b10101111;
    4'b1011: hex_code <= 8'b11101100;
    4'b1100: hex_code <= 8'b11001000;
    4'b1101: hex_code <= 8'b11101001;
    4'b1110: hex_code <= 8'b11001110;
    default: hex_code <= 8'b10001110;
  endcase
end

endmodule

`endif
