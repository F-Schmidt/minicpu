/*
 * Copyright (c) 2020. Falko Schmidt
 * Behavior-based description of a register, that shifts the input data
 * 4 bits to the left and fills up the lower bits with '0'.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _SL_Register_v
`define _SL_Register_v

module SL_Register (
  input [3:0]D_IN,
  output [7:0]D_OUT);

assign D_OUT[7:4] = D_IN;
assign D_OUT[3:0] = 4'b0000;

endmodule // SL_Register

`endif
