/*
 * Copyright (c) 2020. Falko Schmidt
 * Behavior-based description of a register, that zero-extends
 * a 4 bit wide binary number to 8 bits.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _ZE_Register_v
`define _ZE_Register_v

module ZE_Register (
  input [3:0]D_IN,
  output [7:0]D_OUT);

assign D_OUT = D_IN | 8'b00000000;

endmodule // ZE_Register

`endif
