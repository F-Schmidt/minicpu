/*
 * Copyright (c) 2020. Falko Schmidt
 * Behavior-based description of the CPU statusflags for overflow,
 * zeroflag, sign and carry.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _StatusFlags_v
`define _StatusFlags_v

module StatusFlags (
  input [7:0]A,
  input [7:0]B,
  input [7:0]S,
  input C_IN,
  input AddSub,
  output reg ZF,
  output CF,
  output SF,
  output OF);

// Get most significant Bits
wire A_MSB, B_MSB, S_MSB;
assign A_MSB = A[7];
assign B_MSB = B[7];
assign S_MSB = S[7];

// Either A or B is signed
wire SGN_A_OR_B;
assign SGN_A_OR_B = (A_MSB ^ B_MSB) ^ AddSub;

// Overflow-Flag. If the sum has a different sign, than the operands,
// then this flag will be '0' indicating, that the result could be invalid.
// Otherwise this flag is '1'.
assign OF = SGN_A_OR_B ~| (~SGN_A_OR_B & (A_MSB ~^ S_MSB));

// Carry Flag. One, if AddSub is '1' and C_IN is '0' or the other way around.
assign CF = AddSub ^ C_IN;

// Sign Flag. This flag is '1' whenever the most significant bit of S is '1'
// and zero, if the MSB of S is '0'. (Sign of S)
assign SF = S_MSB;

// Zero Flag. This flag is '1' if, and only if, S is zero (8'b00000000).
// Otherwise it is '0'
always @ ( S ) begin
  if(S == 8'b00000000) begin
    ZF = 1'b1;
  end else begin
    ZF = 1'b0;
  end
end

endmodule // Statusflags

`endif
