/*
 * Copyright (c) 2020. Falko Schmidt
 * Structural description of an 8 bit wide register that uses eight
 * gated D-latches.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _Register_v
`define _Register_v

`include "Gated_DLatch.v"

module Register (
  input [7:0]D,
  input CLK,
  output [7:0]D_OUT
  );

  genvar i;
  for(i = 0; i < 8; i = i+1) begin
    Gated_DLatch G0 (.D(D[i]), .Clk(CLK), .Q(D_OUT[i]));
  end

endmodule //Register

`endif
