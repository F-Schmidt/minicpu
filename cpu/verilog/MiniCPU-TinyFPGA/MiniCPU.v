/*
 * Copyright (c) 2020. Falko Schmidt
 * Main module of the MiniCPU in Verilog.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`include "I2C_Slave.v"
`include "Debouncer.v"
`include "InstructionSplitter.v"
`include "AdderSubtractor.v"
`include "StatusFlags.v"
`include "ZE_Register.v"
`include "SL_Register.v"
`include "Registerbank.v"
`include "Registermonitor.v"
`include "I2C_Slave.v"

module MiniCPU (
  // Instruction and clock inputs
  input [7:0] instruction_input,
  input CLK_input,

  // CPU flag outputs
  output ZF,
  output CF,
  output SF,
  output OF,

  // Clock for shift registers
  input clk_16Mhz,

  // USB pull-up
  output USBPU,

  // Shift register outputs
  output shiftout_clock0,
  output shiftout_clock1,
  output shiftout_clock2,
  output shiftout_clock3,

  output shiftout_latch0,
  output shiftout_latch1,
  output shiftout_latch2,
  output shiftout_latch3,

  output shiftout_data0,
  output shiftout_data1,
  output shiftout_data2,
  output shiftout_data3,

  // SPI-Interface to Computer
  //output SPI_MISO,
  //input SPI_MOSI,
  //input SPI_SS,
  //input SPI_SCK
  input I2C_SCL,
  inout I2C_SDA,
  output I2C_GND);

// Drive USB pull-up to '0' to disable USB
assign USBPU = 0;

// The clock signal must be debounced due to mechanical bouncing.
// The clock input signal will be filtered and output to CLK.
wire CLK;
Debouncer DCLK (.pb_1(CLK_input), .clk(clk_16Mhz),  .pb_out(CLK));

// Adder signals
reg [7:0]B;
reg AddSub;
wire [7:0]RB_IN;
wire C_OUT;

// Instruction Splitter signals
wire [1:0]Op;
wire [1:0]X;
wire [1:0]Y;
wire [1:0]Z;
wire [3:0]XY;

// Register bank signals
reg[1:0] src0;
reg[1:0] src1;
wire[7:0] out0;
wire[7:0] out1;

// Util register signals
wire[7:0] SL_OUT;
wire[7:0] ZE_OUT;

wire[7:0] DATA_REG_00;
wire[7:0] DATA_REG_01;
wire[7:0] DATA_REG_02;
wire[7:0] DATA_REG_03;

I2C_Slave ICS0 (I2C_SCL, I2C_SDA, I2C_GND);
// Connect SPI module to computer
// clk, SCK, MOSI, MISO, SSEL
//SPI_slave SS0 (.clk(clk_16Mhz), .SCK(SPI_SCK), .MOSI(SPI_MOSI),
//               .SSEL(SPI_SS), .MISO(SPI_MISO));

// InstructionSplitter module
InstructionSplitter IS0 (.instruction(instruction_input), .X(X), .Y(Y),
    .XY(XY), .Z(Z), .Op(Op));

// Adder module
AdderSubtractor AS0 (.VEC_A(out0), .VEC_B(B), .AddSub(AddSub), .CLK(CLK),
    .VEC_S(RB_IN), .C_OUT(C_OUT));

// Status flag module
StatusFlags SF0 (.A(out0), .B(B), .S(RB_IN), .C_IN(C_OUT), .AddSub(AddSub),
    .ZF(ZF), .CF(CF), .SF(SF), .OF(OF));

// Util registers
ZE_Register ZE0 (.D_IN(XY), .D_OUT(ZE_OUT));
SL_Register SL0 (.D_IN(XY), .D_OUT(SL_OUT));

// Register bank containing all four internal registers. The data will be
// connected to the Registermonitor and to the slave module for computer
// interaction.
Registerbank RB0 (.D_IN(RB_IN), .CLK(CLK), .dest(Z), .src0(src0), .src1(src1),
    .out0(out0), .out1(out1), .DATA_REG_00(DATA_REG_00),
    .DATA_REG_01(DATA_REG_01), .DATA_REG_02(DATA_REG_02),
    .DATA_REG_03(DATA_REG_03));

// Outputs the register data to a Hexdisplay-encoder and then passes those
// values out to the Hexdisplays.
Registermonitor RM0 (.reg0(DATA_REG_00), .reg1(DATA_REG_01), .reg2(DATA_REG_02),
    .reg3(DATA_REG_03), .clk_16Mhz(clk_16Mhz),
    .shiftout_clock0(shiftout_clock0), .shiftout_clock1(shiftout_clock1),
    .shiftout_clock2(shiftout_clock2), .shiftout_clock3(shiftout_clock3),
    .shiftout_latch0(shiftout_latch0), .shiftout_latch1(shiftout_latch1),
    .shiftout_latch2(shiftout_latch2), .shiftout_latch3(shiftout_latch3),
    .shiftout_data0(shiftout_data0), .shiftout_data1(shiftout_data1),
    .shiftout_data2(shiftout_data2), .shiftout_data3(shiftout_data3));

// Set src0, src1, B and AddSub according to the Op-code
always @ ( * ) begin
  if(Op == 2'b00) begin
    src0 = 2'b00;
    src1 = 2'b00;
    B = ZE_OUT;
    AddSub = 1'b0;
  end else if(Op == 2'b01) begin
    src0 = Z;
    src1 = 2'b00;
    B = SL_OUT;
    AddSub = 1'b0;
  end else if(Op == 2'b10) begin
    src0 = X;
    src1 = Y;
    B = out1;
    AddSub = 1'b0;
  end else begin
    src0 = X;
    src1 = Y;
    B = out1;
    AddSub = 1'b1;
  end
end

endmodule // MiniCPU
