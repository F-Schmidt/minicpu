/*
 * Copyright (c) 2020. Falko Schmidt
 * Behavioral description of a controllable inverter. If the selection
 * is set to '1', then all bytes of the input vector will be negated.
 * Otherwise the output is the same to the input value.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 `ifndef _ControllableInverter_v
 `define _ControllableInverter_v

module ControllableInverter (
  input [7:0]D_IN,
  input SEL,
  output reg [7:0]D_OUT);

  // If SEL is '1' then the output will be negated,
  // otherwise the value itself will be output.
  always @ ( SEL ) begin
    if (SEL == 1'b1)
      D_OUT = ~D_IN;
    else
      D_OUT = D_IN;
  end
endmodule //ControllableInverter

`endif
