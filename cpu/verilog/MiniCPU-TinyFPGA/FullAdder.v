/*
 * Copyright (c) 2020. Falko Schmidt
 * Structural description of a single bit Fulladder.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _FullAdder_v
`define _FullAdder_v

`include "HalfAdder.v"
`include "OR_Gate.v"

module FullAdder (
  input A, B, C_IN,
  output S, C_OUT);

// Wires to connect the signals internally
wire SUM_FIRST_HA, SUM_SECOND_HA;
wire CARRY_FIRST_HA, CARRY_SECOND_HA;

HalfAdder HA0 (.A(A), .B(B), .S(SUM_FIRST_HA), .C_OUT(CARRY_FIRST_HA));
HalfAdder HA1 (.A(C_IN), .B(SUM_FIRST_HA), .S(S), .C_OUT(CARRY_SECOND_HA));
OR_Gate   OR0 (.A(CARRY_FIRST_HA), .B(CARRY_SECOND_HA), .Y(C_OUT));

endmodule

`endif
