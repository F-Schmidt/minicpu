/*
 * Copyright (c) 2020. Falko Schmidt
 * Description of a Registermonitor, which displays the content of all four
 * implemented registers on 7 segment displays using shift registers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _Registermonitor_v
`define _Registermonitor_v

`include "HexDisplayDriver.v"
`include "ShiftRegisterDriver.v"

module Registermonitor (input[7:0] reg0,
                        input[7:0] reg1,
                        input[7:0] reg2,
                        input[7:0] reg3,
                        input clk_16Mhz,
                        output shiftout_clock0,
                        output shiftout_clock1,
                        output shiftout_clock2,
                        output shiftout_clock3,
                        output shiftout_latch0,
                        output shiftout_latch1,
                        output shiftout_latch2,
                        output shiftout_latch3,
                        output shiftout_data0,
                        output shiftout_data1,
                        output shiftout_data2,
                        output shiftout_data3);

// Internal storage to connect register content to BCD encoders
wire[15:0] regVal0, regVal1, regVal2, regVal3;

// Register %0
HexDisplayDriver hex00 (.value(reg0[3:0]), .hex_code(regVal0[15:8]));
HexDisplayDriver hex01 (.value(reg0[7:4]), .hex_code(regVal0[7:0]));
ShiftRegisterDriver srd0 (.clk_16Mhz(clk_16Mhz), .data(regVal0),
    .shiftout_clock(shiftout_clock0), .shiftout_latch(shiftout_latch0),
    .shiftout_data(shiftout_data0));

// Register %1
HexDisplayDriver hex10 (.value(reg1[3:0]), .hex_code(regVal1[15:8]));
HexDisplayDriver hex11 (.value(reg1[7:4]), .hex_code(regVal1[7:0]));
ShiftRegisterDriver srd1 (.clk_16Mhz(clk_16Mhz), .data(regVal1),
    .shiftout_clock(shiftout_clock1), .shiftout_latch(shiftout_latch1),
    .shiftout_data(shiftout_data1));

// Register %2
HexDisplayDriver hex20 (.value(reg2[3:0]), .hex_code(regVal2[15:8]));
HexDisplayDriver hex21 (.value(reg2[7:4]), .hex_code(regVal2[7:0]));
ShiftRegisterDriver srd2 (.clk_16Mhz(clk_16Mhz), .data(regVal2),
    .shiftout_clock(shiftout_clock2), .shiftout_latch(shiftout_latch2),
    .shiftout_data(shiftout_data2));

// Register %3
HexDisplayDriver hex30 (.value(reg3[3:0]), .hex_code(regVal3[15:8]));
HexDisplayDriver hex31 (.value(reg3[7:4]), .hex_code(regVal3[7:0]));
ShiftRegisterDriver srd3 (.clk_16Mhz(clk_16Mhz), .data(regVal3),
    .shiftout_clock(shiftout_clock3), .shiftout_latch(shiftout_latch3),
    .shiftout_data(shiftout_data3));

endmodule // RegisterMonitor

`endif
