// From https://www.fpga4fun.com/SPI2.html
`ifndef _SPI_slave_v
`define _SPI_slave_v

module SPI_slave(input clk, SCK, MOSI, SSEL,
                 output MISO);

// ----------------------------------------------------------------------------------------------------
// sync SCK to the FPGA clock using a 3-bits shift register
// ----------------------------------------------------------------------------------------------------
reg [2:0] SCKr = 0;

always @(posedge clk) begin
  SCKr <= {SCKr[1:0], SCK};
end

wire SCK_risingedge = (SCKr[2:1]==2'b01);  // now we can detect SCK rising edges
wire SCK_fallingedge = (SCKr[2:1]==2'b10);  // and falling edges

// same thing for SSEL
reg [2:0] SSELr;

always @(posedge clk) begin
  SSELr <= {SSELr[1:0], SSEL};
end

wire SSEL_active = ~SSELr[1];  // SSEL is active low
wire SSEL_startmessage = (SSELr[2:1]==2'b10);  // message starts at falling edge
wire SSEL_endmessage = (SSELr[2:1]==2'b01);  // message stops at rising edge

// and for MOSI
reg [1:0] MOSIr;

always @(posedge clk) begin
  MOSIr <= {MOSIr[0], MOSI};
end

// Das nächste empfangene Bit
wire MOSI_data = MOSIr[1];

// ----------------------------------------------------------------------------------------------------
// we handle SPI in 8-bits format, so we need a 3 bits counter to count the bits as they come in
// ----------------------------------------------------------------------------------------------------
// Läuft immer von 0 bis 7
reg [2:0] bitcnt;

// Wenn der Bitcounter auf 7 ist, wurden 8 Bits empfangen -> diese Variable
// wird auf HIGH gesetzt.
reg byte_received;  // high when a byte has been received

// Die empfangenen Daten
reg [7:0] byte_data_received;

// Wenn Chipselect low ist, dann geht die Übertragung los und der Bitcounter wird auf 0 gesetzt.
// Wenn SCK eine rising edge aufweist, dann wird der Bitcounter erhöht und das Bit wird gespeichert.
always @(posedge clk)
begin
  if(~SSEL_active) begin
    bitcnt <= 3'b000;
  end else if(SCK_risingedge) begin
    bitcnt <= bitcnt + 3'b001;
    // implement a shift-left register (since we receive the data MSB first)
    byte_data_received <= {byte_data_received[6:0], MOSI_data};
  end
end

// Wenn Chipselect wieder high ist und die Clock gerade ansteigt und der Bitcount
// auf 7 ist, dann wurde ein Byte empfangen
always @(posedge clk) begin
  byte_received <= SSEL_active && SCK_risingedge && (bitcnt==3'b111);
end

// Hier werden alle Aktionen definiert. Welche Daten werden herausgegeben, bei
// dem jeweils gesendeten Code?
reg[7:0] to_be_sent;

always @(posedge clk) begin
  if(byte_received == 8'b00000001) begin
      to_be_sent <= 8'b00001111;
      //TODO implement codes
  end else if(byte_received == 8'b00010001) begin
      to_be_sent <= 8'b10000000;
  end
end


// ----------------------------------------------------------------------------------------------------
// SENDING
// ----------------------------------------------------------------------------------------------------

// Die Daten, die gesendet werden sollen
reg [7:0] byte_data_sent = 8'b00010001;

// Counter für was?
//reg [7:0] cnt;
//always @(posedge clk) if(SSEL_startmessage) cnt<=cnt+8'h1;  // count the messages

always @(posedge clk)
// Wenn CS HIGH ist, dann
if(SSEL_active)
begin
  // Wenn eine Nachricht beginnt, dann sende die Anzahl der Nachrichten
  if(SSEL_startmessage) begin
    // first byte sent in a message is the message count
    byte_data_sent <= {byte_data_sent[6:0], MOSI_data};
  end
    //byte_data_sent <= cnt;
    //byte_data_sent <= 8'hEF;
  //else
  // Wenn die Clock fällt...
//  if(SCK_fallingedge)
//  begin
    // Wenn der Bitcounter auf 0 ist, dann sende Nullen
  //  if(bitcnt==3'b000)
    //  byte_data_sent <= 8'h00;  // after that, we send 0s
    //else
      // Sonst sende mindestens mal eine 1
      //byte_data_sent <= {byte_data_sent[6:0], 1'b0};
      //byte_data_sent <= 8'd42;
  //end
end

// Sende immer das höchste Bit
assign MISO = byte_data_sent[7];  // send MSB first
// we assume that there is only one slave on the SPI bus
// so we don't bother with a tri-state buffer for MISO
// otherwise we would need to tri-state MISO when SSEL is inactive

endmodule

`endif
