/*
 * Copyright (c) 2020. Falko Schmidt
 * Structural description of a single bit half adder.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _HalfAdder_v
`define _HalfAdder_v

`include "XOR_Gate.v"
`include "AND_Gate.v"

 module HalfAdder(input A, B,
                  output S, C_OUT);

  XOR_Gate X0 (.A(A), .B(B), .Y(S));
  AND_Gate A0 (.A(A), .B(B), .Y(C_OUT));

endmodule

`endif
