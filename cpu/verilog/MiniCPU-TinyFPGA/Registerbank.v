/*
 * Copyright (c) 2020. Falko Schmidt
 * Register bank module, which has 4 registers. The data is saved via a 2-bit
 * wide register selection. In addition, by setting two registers, each 2 bits
 * wide, any desired register contents can be given to 2 outputs. This register
 * bank contains a zero register (%0) which cannot be written to, but read.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _Registerbank_v
`define _Registerbank_v

`include "Register.v"

module Registerbank (input [7:0]D_IN,

                    // Clock
                    input CLK,

                    // Register input destination
                    input [1:0]dest,

                    // Registerbank output switches
                    input [1:0]src0,
                    input [1:0]src1,

                    // Registerbank outputs
                    output reg [7:0]out0,
                    output reg [7:0]out1,

                    // Output register content for connection to other modules
                    output [7:0]DATA_REG_00,
                    output [7:0]DATA_REG_01,
                    output [7:0]DATA_REG_02,
                    output [7:0]DATA_REG_03);


// Data contents
// This register will always be zero
reg [7:0]DATA_REG_00;
wire [7:0]DATA_REG_01;
wire [7:0]DATA_REG_02;
wire [7:0]DATA_REG_03;


// Input, that might be written
reg [7:0]WR_REG_01;
reg [7:0]WR_REG_02;
reg [7:0]WR_REG_03;

initial begin
  DATA_REG_00 = 8'b00000000;
  WR_REG_01 = 8'b00000000;
  WR_REG_02 = 8'b00000000;
  WR_REG_03 = 8'b00000000;
end

// Registers
Register reg1 (.D(WR_REG_01), .CLK(CLK), .D_OUT(DATA_REG_01));
Register reg2 (.D(WR_REG_02), .CLK(CLK), .D_OUT(DATA_REG_02));
Register reg3 (.D(WR_REG_03), .CLK(CLK), .D_OUT(DATA_REG_03));

// Output 0
always @ ( src0 ) begin
  if(src0 == 2'b00) begin
    out0 = DATA_REG_00;
  end else if(src0 == 2'b01) begin
    out0 = DATA_REG_01;
  end else if (src0 == 2'b10) begin
    out0 = DATA_REG_02;
  end else begin
    out0 = DATA_REG_03;
  end
end

// Output 1
always @ ( src1 ) begin
  if(src1 == 2'b00) begin
    out1 = DATA_REG_00;
  end else if(src1 == 2'b01) begin
    out1 = DATA_REG_01;
  end else if (src1 == 2'b10) begin
    out1 = DATA_REG_02;
  end else begin
    out1 = DATA_REG_03;
  end
end

// Destination / writing
always @ (negedge CLK) begin
  if (dest == 2'b01) begin
    WR_REG_01 = D_IN;
  end else if (dest == 2'b10) begin
    WR_REG_02 = D_IN;
  end else if (dest == 2'b11) begin
    WR_REG_03 = D_IN;
  end else begin
    // DO NOT WRITE INTO DATA_REG_00!
  end
end

endmodule // Registerbank

`endif
