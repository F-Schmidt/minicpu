/*
 * Copyright (c) 2020. Falko Schmidt
 * Structural description of an eight bit wide Adder, that can also execute
 * subtraction.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _AdderSubtractor_v
`define _AdderSubtractor_v

`include "ControllableInverter.v"
`include "BufferedCarryRippleAdder.v"

module AdderSubtractor (input [7:0]VEC_A,
               input [7:0]VEC_B,
               input AddSub,
               input CLK,
               output [7:0] VEC_S,
               output C_OUT);

wire [7:0]ADDER_B_IN;

ControllableInverter CI0 (.D_IN(VEC_B), .SEL(AddSub), .D_OUT(ADDER_B_IN));
BufferedCarryRippleAdder BC0 (.VEC_A(VEC_A), .VEC_B(ADDER_B_IN), .C_IN(AddSub),
                              .CLK(CLK), .VEC_S(VEC_S), .C_OUT(C_OUT));

endmodule

`endif
