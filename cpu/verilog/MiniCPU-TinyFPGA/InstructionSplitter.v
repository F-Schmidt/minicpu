/*
 * Copyright (c) 2020. Falko Schmidt
 * Description of an instruction splitter, which provides separat arguments.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _InstructionSplitter_v
`define _InstructionSplitter_v

module InstructionSplitter (
  input [7:0]instruction,
  output [1:0] X,
  output [1:0] Y,
  output [3:0] XY,
  output [1:0] Z,
  output [1:0] Op
);

assign Op = instruction[7:6];
assign X = instruction[5:4];
assign Y = instruction[3:2];
assign Z = instruction[1:0];
assign XY = instruction[5:2];

endmodule //InstructionSplitter

`endif
