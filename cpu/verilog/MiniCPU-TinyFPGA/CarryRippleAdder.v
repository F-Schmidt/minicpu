/*
 * Copyright (c) 2020. Falko Schmidt
 * Structural description of a 8 bit wide Carry-Ripple-Adder.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _CarryRippleAdder_v
`define _CarryRippleAdder_v

`include "FullAdder.v"

module CarryRippleAdder(
  input [7:0]VEC_A,
  input [7:0]VEC_B,
  input C_IN,
  output [7:0]VEC_S,
  output C_OUT);

wire [8:0]CARRY_INTERNAL;
assign CARRY_INTERNAL[0] = C_IN;
assign C_OUT = CARRY_INTERNAL[8];

genvar i;
for(i = 0; i < 8; i = i+1) begin
  FullAdder FA0 (.A(VEC_A[i]), .B(VEC_B[i]), .C_IN(CARRY_INTERNAL[i]),
                 .S(VEC_S[i]), .C_OUT(CARRY_INTERNAL[i+1]));
end

endmodule

`endif
