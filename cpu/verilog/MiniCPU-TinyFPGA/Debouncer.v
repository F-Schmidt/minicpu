/*
 * Copyright (c) 2020. Falko Schmidt
 * Structural description of a debouncer.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`ifndef _Debouncer_v
`define _Debouncer_v

module Debouncer(input pb_1, clk, output pb_out);
  wire slow_clk;
  wire Q0, Q1, Q2, Q2_neg;

  Divided_clock DC1(clk, slow_clk);
  D_Flipflop DFF0(clk, slow_clk, pb_1, Q0);
  D_Flipflop DFF1(clk, slow_clk, Q0, Q1);
  D_Flipflop DFF2(clk, slow_clk, Q1, Q2);

  assign Q2_neg = ~Q2;
  assign pb_out = ~(Q1 & Q2_neg);

endmodule

// Clock divider, which outputs a slow clock
module Divided_clock(input Clk_16M, output slow_clk);

  reg [26:0]counter = 0;
  always @(posedge Clk_16M) begin
   counter <= (counter >= 39999)?0:counter+1;
  end

  assign slow_clk = (counter == 39999)?1'b1:1'b0;

endmodule


// D-Flipflop module
module D_Flipflop(input DFF_CLOCK, clock_enable, D, output reg Q=0);

  always @ (posedge DFF_CLOCK) begin
  if(clock_enable == 1)
   Q <= D;
  end

endmodule

`endif
